## Application downloader library for Appium automation on Android/iOS real devices

### Dependencies

Add JitPack repository and dependencies to your project:

[![](https://jitpack.io/v/org.bitbucket.ochubey/app-downloader.svg)](https://jitpack.io/#org.bitbucket.ochubey/app-downloader)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/ecb7140f9c7a45c2a9b54bfbe7c62ef8)](https://www.codacy.com/app/chubej/App-Downloader?utm_source=ochubey@bitbucket.org&amp;utm_medium=referral&amp;utm_content=ochubey/app-downloader&amp;utm_campaign=Badge_Grade)

---

## Use library to get a remote link to the application or local path of download application

```java
//...
import ua.in.ochubey.FileDownloader;
//...

public class DeviceCapabilities {
    
    //...
    private static final String HOCKEY_APP_TOKEN = "250b5fb557bs895dabd36fdec49d4ef0";
    private static final String ANDROID_APP_ID = "5430b5fb57bs89sdgdabdfdc49d4ef0y";
    private String url = fileDownloader.getLatestAndroidAppLink(androidAppId);
    //...
    
    public DesiredCapabilities getDesireCapabilities(){
        DesiredCapabilities capabilities;
        //...
        FileDownloader fileDownloader = new FileDownloader(HOCKEY_APP_TOKEN);
        capabilities.setCapability(MobileCapabilityType.APP, fileDownloader.getLatestAndroidAppPath(ANDROID_APP_ID));
        //...
    }
}
```