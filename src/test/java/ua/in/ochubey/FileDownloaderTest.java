package ua.in.ochubey;

import org.apache.http.util.TextUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.stream.Stream;

import static org.junit.Assert.*;
import static org.junit.Assume.assumeNotNull;

public class FileDownloaderTest {

    private String androidAppId;
    private String iosAppId;
    private FileDownloader fileDownloader;
    private static final Logger LOGGER = LoggerFactory.getLogger(FileDownloaderTest.class);

    @Before
    public void setUp() {
        String hockeyAppToken = System.getProperty("token");
        androidAppId = System.getProperty("android");
        iosAppId = System.getProperty("ios");
        assumeNotNull("ConnectionService Token was not provided, please provide it with parameter -Dtoken", hockeyAppToken);
        assumeNotNull("Android App ID was not provided, please provide it with parameter -Dandroid", androidAppId);
        assumeNotNull("iOS App ID Token was not provided, please provide it with parameter -Dios", iosAppId);
        fileDownloader = new FileDownloader(hockeyAppToken);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @After
    public void tearDown() {
        Path path = Paths.get("./downloaded/");
        if (path.toFile().exists()) {
            try (Stream<Path> walk = Files.walk(path)) {
                walk.sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
            } catch (IOException e) {
                LOGGER.trace(e.getMessage(), e);
            }
        }
    }

    @Test
    public void getAndroidAppLink() {
        String url = fileDownloader.getLatestAndroidAppLink(androidAppId);
        assertFalse(TextUtils.isEmpty(url));
        assertTrue(url.startsWith("https"));
        assertTrue(url.endsWith("apk"));
    }

    @Test
    public void getAndroidAppPath() {
        String path = fileDownloader.getLatestAndroidAppPath(androidAppId);
        assertFalse(TextUtils.isEmpty(path));
        assertTrue(path.contains("downloaded"));
        assertTrue(path.endsWith("apk"));
    }

    @Test
    public void getIosAppLink() {
        String url = fileDownloader.getLatestIosAppLink(iosAppId);
        assertFalse(TextUtils.isEmpty(url));
        assertTrue(url.startsWith("https"));
        assertTrue(url.endsWith("ipa"));
    }

    @Test
    public void getIosAppPath() {
        String path = fileDownloader.getLatestIosAppPath(iosAppId);
        assertFalse(TextUtils.isEmpty(path));
        assertTrue(path.contains("downloaded"));
        assertTrue(path.endsWith("ipa"));
    }
}
