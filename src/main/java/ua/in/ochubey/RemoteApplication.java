package ua.in.ochubey;


import static java.lang.String.format;

public class RemoteApplication {

    private Platform platform;
    private String appId;
    private String hockeyAppToken;
    private String downloadUrl;
    private String appTitle;
    private String appVersion;
    private String appPath;

    public RemoteApplication(Platform platform, String appId, String hockeyAppToken, String appTitle, String appVersion) {
        setPlatform(platform);
        setAppId(appId);
        setHockeyAppToken(hockeyAppToken);
        setAppTitle(appTitle);
        setAppVersion(appVersion);
    }

    public void updateApplication(String downloadUrl, String appPath) {
        setDownloadUrl(downloadUrl);
        setAppPath(appPath);
    }

    public Platform getPlatform() {
        return platform;
    }

    private void setPlatform(Platform platform) {
        this.platform = platform;
    }

    public String getAppId() {
        return appId;
    }

    private void setAppId(String appId) {
        this.appId = appId;
    }

    public String getHockeyAppToken() {
        return hockeyAppToken;
    }

    private void setHockeyAppToken(String hockeyAppToken) {
        this.hockeyAppToken = hockeyAppToken;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    private void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getAppTitle() {
        return appTitle;
    }

    private void setAppTitle(String appTitle) {
        this.appTitle = appTitle;
    }

    public String getAppVersion() {
        return appVersion;
    }

    private void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getAppPath() {
        return appPath;
    }

    private void setAppPath(String appPath) {
        this.appPath = appPath;
    }

    @Override
    public String toString() {
        return format("Application [appId='%s', appPath='%s', appTitle='%s', platform='%s', appVersion='%s', downloadUrl='%s', " +
                "hockeyAppToken='%s']", getAppId(), getAppPath(), getAppTitle(), getPlatform(), getAppVersion(), getDownloadUrl(),
                getHockeyAppToken());
    }
}
