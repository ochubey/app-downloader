package ua.in.ochubey;

import org.apache.http.util.TextUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.in.ochubey.schema.AppVersion;
import ua.in.ochubey.schema.AppVersions;
import ua.in.ochubey.schema.AppVersionsSchema;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.text.Normalizer;

import static ua.in.ochubey.Platform.getPlatform;

/**
 * Created by ochubey on 4/9/16.
 */
public class FileDownloader {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileDownloader.class);
    private String hockeyAppToken;

    /**
     * Creates instance of FileDownloader with specific access level granted by provided token
     *
     * @param hockeyAppToken ConnectionService token can be obtained from Account Settings > API Tokens of HokeyApp web page
     */
    public FileDownloader(String hockeyAppToken) {
        this.hockeyAppToken = hockeyAppToken;
    }

    /**
     * Method returns Android application download URL as String value
     *
     * @param appId public_identifier of the application to be downloaded
     * @return Android application download URL as String value
     */
    public String getLatestAndroidAppLink(String appId) {
        return getLatestAppLink(appId, Platform.ANDROID, true);
    }

    /**
     * Method returns local PATH to downloaded Android application as String value
     *
     * @param appId public_identifier of the application to be downloaded
     * @return local PATH to downloaded Android application as String value
     */
    public String getLatestAndroidAppPath(String appId) {
        return getLatestAppLink(appId, Platform.ANDROID, false);
    }

    /**
     * Method returns iOS application download URL as String value
     *
     * @param appId public_identifier of the application to be downloaded
     * @return iOS application download URL as String value
     */
    public String getLatestIosAppLink(String appId) {
        return getLatestAppLink(appId, Platform.IOS, true);
    }

    /**
     * Method returns local PATH to downloaded iOS application as String value
     *
     * @param appId public_identifier of the application to be downloaded
     * @return local PATH to downloaded iOS application as String value
     */
    public String getLatestIosAppPath(String appId) {
        return getLatestAppLink(appId, Platform.IOS, false);
    }

    /**
     * Method return aggregated information about downloadable application based on info of specific HockeyApp account
     *
     * @param appId         public_identifier of the application to be downloaded
     * @param platformValue name of the platform where application would be installed
     * @return aggregated information about downloaded application
     */
    public RemoteApplication getAppLinkByPlatform(String appId, String platformValue) {
        AppVersion appVersion = getLatestApplication(appId);
        if (appVersion != null) {
            String title = unaccent(appVersion.getTitle());
            String version = getLatestAppVersion(appVersion);
            Platform platform = getPlatform(platformValue);
            RemoteApplication application = new RemoteApplication(platform, appId, hockeyAppToken, title, version);
            String downloadUrl = formDownloadLink(appVersion.getDownloadUrl(), application);
            String appPath = formLocalPath(application, downloadUrl);
            LOGGER.warn("downloadUrl: {}", downloadUrl);
            LOGGER.warn("appPath: {}", appPath);
            application.updateApplication(downloadUrl, appPath);
            LOGGER.warn(application.toString());
            return application;
        } else {
            return null;
        }
    }

    private String getLatestAppVersion(AppVersion appVersion) {
        if (appVersion != null) {
            return appVersion.getShortVersion() + "(" + appVersion.getVersion() + ")";
        }
        return null;
    }

    private String getLatestAppLink(String appId, Platform platform, boolean returnUrl) {
        RemoteApplication application = getAppLinkByPlatform(appId, platform.getValue());
        if (application == null) {
            return "";
        }
        if (returnUrl) {
            return application.getDownloadUrl();
        } else {
            return application.getAppPath();
        }
    }

    private String formDownloadLink(String downloadUrl, RemoteApplication application) {
        if (TextUtils.isEmpty(downloadUrl)) {
            return "";
        } else {
            String apiUrl = downloadUrl.replace("/apps/", "/api/2/apps/") + "?format={%s}";

            String apiUrlWithAppFormat;
            Platform platform = application.getPlatform();
            if (platform == Platform.ANDROID) {
                apiUrlWithAppFormat = String.format(apiUrl, "apk");
            } else if (platform == Platform.IOS) {
                apiUrlWithAppFormat = String.format(apiUrl, "ipa");
            } else {
                LOGGER.error("Platform is not supported. Please contribute to add following platform to the list of supported:");
                LOGGER.error(platform.getValue());
                return null;
            }
            try {
                URL url = new URL(apiUrlWithAppFormat);
                return url.toString();
            } catch (MalformedURLException e) {
                LOGGER.trace(e.getMessage(), e);
            }
            return null;
        }
    }

    private String formLocalPath(RemoteApplication application, String downloadUrl) {
        File file;

        String title = unaccent(application.getAppTitle().replace(" ", ""));
        String version = application.getAppVersion().replaceAll("[.()]", "");

        File theDir = new File("downloaded");
        if (!theDir.exists()) {
            try {
                theDir.mkdir();
            } catch (SecurityException se) {
                LOGGER.error("Unable to create download directory", se);
            }
        }

        String filePath = "downloaded/" + title + version;
        Platform platform = application.getPlatform();

        if (platform == Platform.ANDROID) {
            file = new File(filePath + ".apk");
        } else if (platform == Platform.IOS) {
            file = new File(filePath + ".ipa");
        } else {
            LOGGER.error("Platform is not supported. Please contribute to add following platform to the list of supported:");
            LOGGER.error(platform.getValue());
            return "";
        }
        if (file.exists()) {
            LOGGER.debug("Application is already downloaded and following file would be used for testing:");
            LOGGER.debug(file.getAbsolutePath());
        } else {
            try {
                downloadFile(downloadUrl, file);
            } catch (IOException e) {
                LOGGER.error("Unable to download application by the link: {}, due to error: {}", downloadUrl, e.getMessage());
                return "";
            }
            LOGGER.debug("Download link was formed and application is to be downloaded:");
            LOGGER.debug(downloadUrl);
        }
        return file.getAbsolutePath();
    }

    private void downloadFile(String downloadUrl, File file) throws IOException {
        URL url = new URL(downloadUrl);
        try (ReadableByteChannel rbc = Channels.newChannel(url.openStream())) {
            writeToFile(file, rbc);
        } catch (Exception ex) {
            LOGGER.trace(ex.getMessage());
        }
    }

    private void writeToFile(File file, ReadableByteChannel rbc) {
        try (FileOutputStream fos = new FileOutputStream(file.getAbsoluteFile())) {
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        } catch (Exception ex) {
            LOGGER.trace(ex.getMessage());
        }
    }

    private AppVersion getLatestApplication(String appId) {
        AppVersions events = new AppVersionsSchema().getApps(hockeyAppToken, appId);
        if (events == null) {
            LOGGER.debug("ConnectionService request returned no results");
            return null;
        }
        int applicationIndex = 0;
        while (applicationIndex <= events.getAppVersions().size()) {
            AppVersion appVersion = events.getAppVersions().get(applicationIndex);
            //Taking latest (first) version of the application and forming download link
            if (appVersion.getDownloadUrl() != null) {
                return appVersion;
            } else {
                applicationIndex++;
            }
        }
        return null;
    }


    // taken from example of Rafael Ponte: https://gist.github.com/rponte/893494#file-stringutils-java
    private static String unaccent(String src) {
        return Normalizer.normalize(src, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    }
}
