package ua.in.ochubey.schema;

import com.google.gson.annotations.SerializedName;

public class AppVersion {
    @SerializedName("download_url")
    private String downloadUrl;
    @SerializedName("version")
    private String version;
    @SerializedName("shortversion")
    private String shortVersion;
    @SerializedName("title")
    private String title;

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public String getVersion() {
        return version;
    }

    public String getShortVersion() {
        return shortVersion;
    }

    public String getTitle() {
        return title;
    }
}
