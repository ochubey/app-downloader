package ua.in.ochubey.schema;

import ua.in.ochubey.ConnectionService;
import okhttp3.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class AppVersionsSchema {
    private static final Logger LOGGER = LoggerFactory.getLogger(AppVersionsSchema.class);
    private ConnectionService hockeyApp;
    private static final long MAXIMUM_COMMAND_TIMEOUT = 120000;

    public AppVersionsSchema() {
        String apiUrl = "https://rink.hockeyapp.net";

        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(MAXIMUM_COMMAND_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(MAXIMUM_COMMAND_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(MAXIMUM_COMMAND_TIMEOUT, TimeUnit.SECONDS);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(apiUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build())
                .build();

        hockeyApp = retrofit.create(ConnectionService.class);
    }

    public AppVersions getApps(String hockeyAppToken, String appId) {
        Response<AppVersions> thisCall = null;
        try {
            thisCall = hockeyApp.appVersions(hockeyAppToken, appId).execute();
        } catch (IOException e) {
            LOGGER.error("error: An error had happened when trying to establish connection with ConnectionService:\n" +
                    e.getMessage() + Arrays.toString(e.getStackTrace()));
        }

        if (thisCall == null) {
            LOGGER.error("ConnectionService was not established");
            return null;
        } else if (!thisCall.isSuccessful()) {
            LOGGER.error(getConnectionError(thisCall));
        }

        return thisCall.body();
    }

    private String getConnectionError(Response<AppVersions> thisCall) {
        if (!thisCall.isSuccessful()) {
            try {
                return thisCall.errorBody().string();
            } catch (IOException e) {
                LOGGER.trace(e.getMessage(), e);
            }
        }
        return "";
    }
}
