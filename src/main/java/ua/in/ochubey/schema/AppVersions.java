package ua.in.ochubey.schema;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppVersions {
    @SerializedName("app_versions")
    private List<AppVersion> appVersionList;

    public List<AppVersion> getAppVersions() {
        return appVersionList;
    }

    private void setAppVersions(List<AppVersion> appVersions) {
        this.appVersionList = appVersions;
    }

    public AppVersions(List<AppVersion> appVersions) {
        setAppVersions(appVersions);
    }
}
