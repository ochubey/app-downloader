package ua.in.ochubey;

import ua.in.ochubey.schema.AppVersions;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface ConnectionService {
    @GET("/api/2/apps/{appID}/app_versions")
    Call<AppVersions> appVersions(
            @Header("X-HockeyAppToken") String token,
            @Path("appID") String appId);
}
