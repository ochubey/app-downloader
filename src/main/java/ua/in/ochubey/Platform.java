package ua.in.ochubey;

public enum Platform {
    ANDROID("android"), IOS("ios");
    private final String value;

    Platform(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Platform getPlatform(String platformValue) {
        if (ANDROID.getValue().equalsIgnoreCase(platformValue)) {
            return ANDROID;
        } else if (IOS.getValue().equalsIgnoreCase(platformValue)) {
            return IOS;
        } else {
            return null;
        }
    }
}
